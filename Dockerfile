# Python support can be specified down to the minor or micro version
# (e.g. 3.6 or 3.6.3).
# OS Support also exists for jessie & stretch (slim and full).
# See https://hub.docker.com/r/library/python/ for all supported Python
# tags from Docker Hub.
FROM python:3.7.3-stretch

# If you prefer miniconda:
#FROM continuumio/miniconda3

LABEL Name=p2p_filesync Version=0.0.1
EXPOSE 1337

WORKDIR /app/src
ADD ./requirements.txt /requirements.txt

RUN mkdir /filesync_test
# Using pip:
RUN python3.7 -m pip install -r /requirements.txt
CMD ["python3.7", "-m", "p2p_filesync.cli", "--config=data/docker-config.ini"]


# Using pipenv:
#RUN python3 -m pip install pipenv
#RUN pipenv install --ignore-pipfile
#CMD ["pipenv", "run", "python3", "-m", "p2p_filesync"]

# Using miniconda (make sure to replace 'myenv' w/ your environment name):
#RUN conda env create -f environment.yml
#CMD /bin/bash -c "source activate myenv && python3 -m p2p_filesync"
