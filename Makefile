.PHONY: clean standalone-gui standalone-cli upload publish

current_dir := $(shell pwd)
version := $(shell git tag | tail -n 1)
dist_name := p2p_filesync

datafiles := $(shell find src/data -type f)
datafiles_args := $(foreach file,$(datafiles),--add-data $(file):data)

upload: bitbucket_authstring := $(shell cat ~/.config/bitbucket/authstring)
upload: bitbucket_api_version := 2.0
upload: bitbucket_owner := errieman
upload: bitbucket_repos := p2p_filesync

clean:
	-find . -name "*.pyc" -exec rm {} \;
	-find . -name "start_*.py" -exec rm {} \;
	-rm -r build dist
	-rm *.spec

standalone-gui:
	@echo version $(version)

	@cat << EOF > src/p2p_filesync/start_gui.py
	from p2p_filesync.gui import main

	if __name__ == '__main__':
		main()
	EOF

	python3.7 /usr/local/bin/pyinstaller \
		--name "$(dist_name)_gui-linux-$(version)" \
		--onefile \
		--noconsole \
		$(datafiles_args) \
		src/p2p_filesync/start_gui.py

	rm src/p2p_filesync/start_gui.py

standalone-cli:
	@echo version $(version)

	@cat << EOF > src/p2p_filesync/start_cli.py
	from p2p_filesync.cli import main

	if __name__ == '__main__':
		main()
	EOF

	python3.7 /usr/local/bin/pyinstaller \
		--name "$(dist_name)-linux-$(version)" \
		--onefile \
		--noconsole \
		$(datafiles_args) \
		src/p2p_filesync/start_cli.py

	rm src/p2p_filesync/start_cli.py


publish: | standalone-gui standalone-cli upload

upload:
	@echo uploading $(dist_name)_gui-linux-$(version)
	@-curl -X POST --user "$(bitbucket_authstring)" \
		"https://api.bitbucket.org/$(bitbucket_api_version)/repositories/$(bitbucket_owner)/$(bitbucket_repos)/downloads" \
		--form files=@"dist/$(dist_name)_gui-linux-$(version)"

	@echo uploading $(dist_name)-linux-$(version)
	@-curl -X POST --user "$(bitbucket_authstring)" \
		"https://api.bitbucket.org/$(bitbucket_api_version)/repositories/$(bitbucket_owner)/$(bitbucket_repos)/downloads" \
		--form files=@"dist/$(dist_name)-linux-$(version)"
