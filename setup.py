from setuptools import setup

setup(
    use_scm_version={
        'write_to': 'version.txt',
        'tag_regex': r'^(?P<prefix>v)?(?P<version>[^\+]+)(?P<suffix>.*)?$',
    },
    data_files=[
        'src/data/config.ini'
        'src/data/icon.png'
    ]
)
