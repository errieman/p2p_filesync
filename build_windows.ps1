$version = (git tag | tail -n 1)
$entry_script = @"
from p2p_filesync.{0} import main

if __name__ == '__main__':
    main()
"@

Foreach ($t in "gui","cli")
{
    $script = $entry_script -f $t
    Set-Content -path .\src\p2p_filesync\start_$t.py -value $script
}

pyinstaller.exe --clean --name p2p_filesync_gui-windows-$version --onefile --noconsole --noupx --add-data "src\data\icon.png;data" src\p2p_filesync\start_gui.py
pyinstaller.exe --clean --name p2p_filesync-windows-$version --onefile --noupx --add-data "src\data\icon.png;data" src\p2p_filesync\start_cli.py