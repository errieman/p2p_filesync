import logging
import signal
import sys

from .connection import Connection
from .file_list import FileList
from .message_handler import MessageHandler
from .synchronizer import Synchronizer
from .utils import EXIT_MISSING_SETTING, setup

logger = logging.getLogger(__name__)


def handle_signals(synchronizer, connection):
    """Catch terminating signals and stop threads"""
    def terminate(*_):
        synchronizer.stop()
        connection.stop()

    signal.signal(signal.SIGTERM, terminate)
    signal.signal(signal.SIGINT, terminate)


def main():
    config, _ = setup()

    try:
        file_list = FileList(
            config['directory'], config['database'])
        conn = Connection(int(config['port']), config['announce_address'])
        sync = Synchronizer(config['directory'], conn, file_list)
        MessageHandler(
            config['directory'], conn, file_list)
    except KeyError as err:
        logger.error('Missing setting: %s', err)
        sys.exit(EXIT_MISSING_SETTING)

    conn.start()
    sync.start()

    handle_signals(sync, conn)

    sync.join()
    conn.join()


if __name__ == '__main__':
    main()
