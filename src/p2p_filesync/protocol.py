import json
import struct
from functools import partial
from io import BytesIO

BUFF_SIZE = 1024 * 8  # 8kB

# message types
MSG_ERROR = b'\x00'
MSG_GET_FILELIST = b'\x01'
MSG_GET_CHECKSUM = b'\x02'
MSG_GET_TIME = b'\x03'
MSG_SET_TIME = b'\x04'
MSG_GET_FILE = b'\x05'
MSG_SEND_FILE = b'\x06'
MSG_DELETE_FILE = b'\x07'
MSG_MOVE_FILE = b'\x08'
MSG_DELETE_DIR = b'\x09'
MSG_MOVE_DIR = b'\x0A'
MSG_SYNC = b'\x0B'
MSG_RESPONSE = b'\x0C'
MSG_RESPONSE_OK = b'\x0D'

# content types
CONT_NOCONT = b'\x00'
CONT_JSON = b'\x01'
CONT_STR = b'\x02'
CONT_FILE = b'\x03'

ERR_UNKNOWN_MESSAGE = 'err_unknown_message'


def get_data(sock, size, content_type):
    """Get all data for a message.

    :returns: data processed appropriately to content_type
    """
    data = b''
    bytes_recvd = 0

    while bytes_recvd < size:
        chunk_size = min(BUFF_SIZE, size - bytes_recvd)
        chunk = sock.recv(chunk_size)
        data += chunk
        bytes_recvd += len(chunk)

    data = data.decode('utf-8')

    if content_type == CONT_JSON:
        data = json.loads(data)

    return data


def parse(sock, addr):
    """Parse an incoming message.

    :param sock: socket to read from
    :param addr: address of peer that sent the message
    :returns: a Message instance
    """
    message_type = sock.recv(1)
    content_type = sock.recv(1)
    message_data_len = struct.unpack('!Q', sock.recv(8))[0]

    if content_type == CONT_FILE:
        path_length = struct.unpack('!i', sock.recv(4))[0]
        path = sock.recv(path_length).decode('utf-8')
        mtime = struct.unpack('!d', sock.recv(8))[0]
        message_data = sock
        return FileMessage(
            message_type, content_type, data=message_data,
            size=message_data_len, addr=addr, path=path, mtime=mtime)

    message_data = get_data(sock, message_data_len, content_type)
    return Message(message_type, content_type, data=message_data,
                        size=message_data_len, addr=addr)


class BaseMessage:
    def __init__(
            self, mtype, content_type=CONT_NOCONT, data='', addr=None, size=0):
        self.mtype = mtype
        self.content_type = content_type
        self.data = data
        self.size = size
        self.addr = addr

    def _message_headers(self):
        """Turn Message information into binary data.

        :returns: headers as bytes
        """
        headers = self.mtype
        headers += self.content_type
        headers += struct.pack('!Q', self.size)
        return headers

    def __str__(self):
        var_gen = ('{}={}'.format(*i) for i in vars(self).items())
        return '<{}: {}>'.format(type(self).__name__, ', '.join(var_gen))

    def write(self, sock):
        raise NotImplementedError()


class FileMessage(BaseMessage):
    """Message that holds a file."""
    def __init__(self, mtype, content_type=CONT_FILE, data=None,
                 size=0, addr=None, path=None, mtime=None):
        super().__init__(
            mtype=mtype, content_type=content_type,
            data=data, size=size, addr=addr)
        self.path = path
        self.mtime = mtime

    def _file_headers(self):
        path_b = self.path.encode('utf-8')
        path_len_b = struct.pack('!i', len(path_b))
        mtime = struct.pack('!d', self.mtime)
        return self._message_headers() + path_len_b + path_b + mtime

    def read(self, size):
        """Read file data from socket."""
        return self.data.recv(size)

    def write(self, sock):
        """Write message to socket.

        :param sock: socket to write to
        """
        headers = self._file_headers()

        sock.sendall(headers)

        with self.data.open() as reader:
            for chunk in iter(partial(reader.read, BUFF_SIZE), b''):
                sock.sendall(chunk)


class Message(BaseMessage):
    """Standard message."""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.size == 0:
            self.size = len(self.data)

    def write(self, sock):
        """Write message to socket.

        :param sock: socket to write to
        """
        headers = self._message_headers()
        sock.sendall(headers)

        with BytesIO(self.data.encode('utf-8')) as stream:
            for chunk in iter(partial(stream.read, BUFF_SIZE), b''):
                sock.sendall(chunk)


class MessageOK(Message):
    def __init__(self):
        super().__init__(MSG_RESPONSE_OK)


class MessageError(Message):
    def __init__(self, data=''):
        super().__init__(MSG_ERROR, CONT_STR, data)