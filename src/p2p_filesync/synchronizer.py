#!/usr/bin/python3
"""Peer to peer file synchronizer"""

import json
import logging
import queue
import threading

from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer

from . import connection, protocol
from .utils import wait_for_write

logger = logging.getLogger(__name__)


def request_checksum(conn, peer_addr, path):
    """Request checksum of a file

    :param connection: connection.Connection instance
    :param peer_addr: address of peer request from
    :param path: relative path of file
    """
    message = protocol.Message(
        protocol.MSG_GET_CHECKSUM, protocol.CONT_STR, data=path)

    try:
        response = conn.send_message(peer_addr, message)
    except connection.Error as err:
        logger.error('could not retrieve checksum: %s', err)
        return None

    if response.mtype == protocol.MSG_ERROR:
        logger.error('could not retrieve checksum: %s', response.data)
        return None

    return response.data


def request_mtime(conn, peer_addr, path):
    """Request the modified time of a file

    :param connection: connection.Connection instance
    :param peer_addr: address of peer request from
    :param path: relative path of file
    """
    message = protocol.Message(
        protocol.MSG_GET_TIME, protocol.CONT_STR, data=path)

    try:
        response = conn.send_message(peer_addr, message)
    except connection.Error as err:
        logger.error('could not retrieve mtime: %s', err)
        return 0.0

    if response.mtype == protocol.MSG_ERROR:
        logger.error('could not retrieve mtime: %s', response.data)
        return 0.0

    return float(response.data)


def send_file(conn, peer_addr, file):
    """Send file to single peer

    :param connection: connection.Connection instance
    :param peer_addr: address of peer to send file to
    :param file: file object
    """
    file_msg = protocol.FileMessage(
        protocol.MSG_SEND_FILE, data=file,
        path=file.path, mtime=file.get_mtime(), size=file.get_size())

    try:
        conn.send_message(peer_addr, file_msg)
    except connection.Error as err:
        logger.error('could not send file: %s', err)


def get_file(conn, peer_addr, file):
    """Download a file from another peer.

    :param peer_addr: address to download from
    :param file: file object to download to
    """
    message = protocol.Message(
        protocol.MSG_GET_FILE, protocol.CONT_STR, data=file.path)

    try:
        response = conn.send_message(peer_addr, message)
    except connection.Error as err:
        logger.error('Could not retrieve file %s: %s', file.path, err)
        return

    if response.mtype == protocol.MSG_ERROR:
        logger.error(
            'Could not retrieve file %s: %s', file.path, response.data)
        return

    file.abs_path.parent.mkdir(parents=True, exist_ok=True)
    with file.open('wb') as f_obj:
        file_len = response.size
        bytes_recvd = 0

        while bytes_recvd < file_len:
            chunk_size = min(protocol.BUFF_SIZE, file_len - bytes_recvd)
            chunk = response.read(chunk_size)

            f_obj.write(chunk)
            bytes_recvd += len(chunk)

    file.set_mtime(response.mtime)


class FileChangeHandler(FileSystemEventHandler):
    """Handles any changes in the watch directory and updates the other peers
    """

    def __init__(self, conn, file_list):
        self.connection = conn
        self.file_list = file_list

    def on_any_event(self, event):
        logger.debug('Triggered {}'.format(event))

    def on_created(self, event):
        if event.is_directory:
            # Don't create empty directories
            return

        rel_path = self.file_list.to_rel_path(event.src_path)
        logger.info('{} has been created'.format(rel_path))

        try:
            with self.file_list:
                file = self.file_list.add(rel_path)
        except FileExistsError:
            logger.debug('Can not create: file {} exists'.format(rel_path))
            return

        wait_for_write(file)

        with file, self.connection:
            file_message = protocol.FileMessage(
                protocol.MSG_SEND_FILE, data=file,
                path=file.path, mtime=file.get_mtime(), size=file.get_size())
            self.connection.broadcast_message(file_message)

    def on_deleted(self, event):
        path = event.src_path
        rel_path = self.file_list.to_rel_path(path)

        # TODO: fix this dirty hack to make deleting dirs work on windows
        if rel_path not in self.file_list:
            event.is_directory = True

        if event.is_directory:
            with self.file_list:
                self.file_list.update()

            delete_message = protocol.Message(
                protocol.MSG_DELETE_DIR, protocol.CONT_STR,
                data=self.file_list.to_rel_path(path))
        else:
            logger.info('%s has been deleted', rel_path)

            try:
                file = self.file_list[rel_path]
            except FileNotFoundError:
                logger.debug(
                    'Can not delete: file %s has already been deleted',
                    rel_path)
                return

            with self.file_list:
                try:
                    self.file_list.remove(rel_path)
                except FileNotFoundError:
                    logger.error('File not in list %s', file.path)

            delete_message = protocol.Message(
                protocol.MSG_DELETE_FILE, protocol.CONT_STR,
                data=file.path)

        with self.connection:
            self.connection.broadcast_message(delete_message)

    def on_moved(self, event):
        move_info = {
            'src_path': self.file_list.to_rel_path(event.src_path),
            'dest_path': self.file_list.to_rel_path(event.dest_path)
        }
        logger.info('File {src_path} moved to {dest_path}'.format(**move_info))

        if event.is_directory:
            with self.file_list:
                self.file_list.update()

            message = protocol.Message(
                protocol.MSG_MOVE_DIR, protocol.CONT_JSON,
                data=json.dumps(move_info))
        else:
            with self.file_list:
                try:
                    self.file_list.move(
                        move_info['src_path'], move_info['dest_path'])
                except FileNotFoundError:
                    # the file has already been moved
                    return

            message = protocol.Message(
                protocol.MSG_MOVE_FILE, protocol.CONT_JSON,
                data=json.dumps(move_info))

        with self.connection:
            self.connection.broadcast_message(message)

    def on_modified(self, event):
        if event.is_directory:
            return

        rel_path = self.file_list.to_rel_path(event.src_path)

        try:
            file = self.file_list[rel_path]
        except FileNotFoundError:
            return

        wait_for_write(file)

        if file.updated_by_synchronizer():
            return

        with file, self.connection:
            logger.info('{} has been modified'.format(rel_path))
            checksum_request = protocol.Message(
                protocol.MSG_GET_CHECKSUM,
                protocol.CONT_STR, data=rel_path)
            local_checksum = file.hash
            responses = self.connection.broadcast_message(checksum_request)

            for response in responses:
                if response.data != local_checksum:
                    send_file(self.connection, response.addr, file)
                else:
                    file_info = {
                        'path': rel_path,
                        'mtime': file.get_mtime()
                    }
                    message = protocol.Message(
                        protocol.MSG_SET_TIME, protocol.CONT_JSON,
                        data=json.dumps(file_info))

                    try:
                        self.connection.send_message(
                            response.addr, message)
                    except connection.Error as err:
                        logger.error(
                            'Could not update mtime of %s on %s: %s',
                            rel_path, response.addr, err)


class Synchronizer(threading.Thread):
    def __init__(self, sync_dir, conn, file_list):
        super().__init__(name='Synchronizer')
        self.sync_dir = sync_dir
        self.connection = conn
        self.file_list = file_list

        self.file_observer = None
        self._sync_queue = queue.Queue()
        self._stop_event = threading.Event()
        self._sync_job_event = threading.Event()

    def run(self):
        """Synchronizes files with other peers
        """
        self.connection.add_on_peer_connected(self.add_sync_job)

        file_change_handler = FileChangeHandler(
            self.connection, self.file_list)
        self.file_observer = Observer()
        self.file_observer.schedule(
            file_change_handler, self.sync_dir, recursive=True)

        try:
            self.file_observer.start()
            logger.debug('Watching file changes')
        except OSError:
            # TODO: make compatible with GUI
            logger.error('Path %s does not exist', self.sync_dir)
            self.connection.stop()
            self.connection.join()
            return

        while not self._stop_event.is_set():
            self._sync_job_event.wait()

            while not self._sync_queue.empty():
                new_peer = self._sync_queue.get_nowait()
                self.sync_files_with(new_peer)

            self._sync_job_event.clear()

    def add_sync_job(self, peer):
        self._sync_queue.put(peer)
        self._sync_job_event.set()

    def sync_files_with(self, peer):
        logger.debug('Syncing with %s', peer.ip_address)

        with self.file_list:
            self.file_list.update()

        message = protocol.Message(protocol.MSG_GET_FILELIST)

        try:
            response = self.connection.send_message(peer.ip_address, message)
        except connection.Error as err:
            logger.error(
                'Synchronization with %s failed: %s', peer.ip_address, err)
            return

        remote_files = response.data
        missing_remote = self.file_list - remote_files
        missing_local = remote_files - self.file_list

        logger.debug('missing_local: %s', missing_local)
        self.get_files(response.addr, missing_local)

        for path in self.file_list - missing_remote - missing_local:
            if self._stop_event.is_set():
                return

            file = self.file_list[path]
            local_mtime = int(file.get_mtime())
            if (path in remote_files and not
                    local_mtime - 4
                    < int(remote_files[path])
                    < local_mtime + 4):
                self.get_updated_file(response.addr, file, remote_files[path])

        logger.info('Done syncing with %s', peer.ip_address)
        peer.synced = True

    def get_files(self, peer_addr, file_list):
        """Download files in file_list from other peer

        :param file_list: a dictionary with file paths as keys
        """
        with self.file_list:
            failed = []

            for path in file_list:
                if self._stop_event.is_set():
                    return

                file = self.file_list.add(path)
                with file:
                    try:
                        get_file(self.connection, peer_addr, file)
                    except (connection.Error, OSError) as err:
                        logger.error(
                            'Error while downloading %s: %s', file.path, err)
                        failed.append(file.path)

            for path in failed:
                self.file_list.remove(path)

    def get_updated_file(self, peer_addr, file, remote_mtime):
        """Compares the checksums of the remote and local file and updates
        local file when they differ or updates local mtime if they're the same
        """
        remote_checksum = request_checksum(
            self.connection, peer_addr, file.path)
        local_checksum = file.hash

        with file:
            if remote_checksum != local_checksum:
                try:
                    get_file(self.connection, peer_addr, file)
                except (connection.Error, OSError) as err:
                    logger.error(
                        'Error while downloading %s: %s', file.path, err)
                    # remove incomplete file
            else:
                file.set_mtime(remote_mtime)

    def stop(self):
        logger.debug('Stopping synchronizer')
        self._stop_event.set()
        # tell sync job loop to continue in order to hit the _stop_event
        self._sync_job_event.set()
        self.file_observer.stop()
        self.file_observer.join()
