import socket
import threading


class ClientSocketDebugAdapter:
    _debug_lock = threading.Lock()

    def __init__(self, sock):
        self.sock = sock

    def recv(self, size):
        with self._debug_lock:
            return self.sock.recv(size)

    def send(self, data):
        with self._debug_lock:
            return self.sock.send(data)

    def sendall(self, data):
        with self._debug_lock:
            return self.sock.sendall(data)

    def fileno(self):
        return self.sock.fileno()


class SocketDebugAdapter(socket.socket):
    _debug_lock = threading.Lock()

    def recv(self, size):
        with self._debug_lock:
            s = socket.fromfd(self.fileno(), socket.AF_INET,
                              socket.SOCK_STREAM)
            return s.recv(size)
