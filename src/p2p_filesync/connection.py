"""The peer to peer part of the program"""
import logging
import socket
import struct
import threading
import time
from inspect import getframeinfo, stack

from . import protocol
from .utils import can_read

ANNOUNCE_MSG = b'A'

logger = logging.getLogger(__name__)


class Error(Exception):
    """Error raised by this module"""


class UnknownPeerError(Error):
    """Raised when peer is not in peer list"""


class PeerDisconnectedError(Error):
    """Peer has disconnected unexpectedly"""

    def __init__(self, msg, peer_addr=None, sock=None):
        super().__init__(msg)
        self.peer_addr = peer_addr
        self.sock = sock


class Client:
    """Connection to a remote peer's server socket"""
    def __init__(self, ip_address, port, change_handlers):
        self.ip_address = ip_address
        self.port = port
        self._change_handlers = change_handlers

        self.last_announcement = time.time()
        self._synced = False

        self._connect()

    @property
    def synced(self):
        return self._synced

    @synced.setter
    def synced(self, value):
        if not isinstance(value, bool):
            raise ValueError('Synced needs to be boolean')

        self._synced = value

        for handler in self._change_handlers:  # TODO: make event queue to be able to handle events asynchronously
            handler(self)

    def _connect(self):
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.settimeout(120)
        self._sock.connect((self.ip_address, self.port))

    def send(self, message):
        """Send a message to the connected peer

        :param message: a message object to send
        """
        message.write(self._sock)

    def get_response(self, timeout=60):
        """Read and parse a response to a message sent previously"""
        if can_read(self._sock, timeout):
            return protocol.parse(self._sock, self.ip_address)

        return None

    def close(self):
        # self.send()
        self._sock.close()


class Server:
    """Server other peers can connect to"""
    def __init__(self, port):
        self.port = port

        self.clients = []
        self._sock = None
        self._responses = {}
        self._stop_event = threading.Event()

        self._setup()

    def _setup(self):
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._sock.bind(('', self.port))
        self._sock.listen()
        self._sock.settimeout(120)
        logger.debug('Listening on %d', self.port)
        self.connection_handler = threading.Thread(
            target=self.handle_connections,
            args=(), name='Connection handler')
        self.connection_handler.setDaemon(True)
        self.connection_handler.start()

    def handle_connections(self):
        """Handle incoming connections of new peers"""
        while not self._stop_event.is_set():
            if can_read(self._sock, 0.1) and not self._stop_event.is_set():
                client = self._sock.accept()
                self.clients.append(client)

    def recv_messages(self):
        """Returns a generator which returns all messages received from
        connected peers

        :returns: 2-tuple containing client socket and parsed message
        """
        for sock, addr in self.clients:
            if sock.fileno() > -1 and can_read(sock, 0):
                try:
                    yield (sock, protocol.parse(sock, addr[0]))
                except struct.error as err:
                    logger.error(
                        'unable to parse message from {}. '
                        'Assuming peer has crashed.'
                        .format(addr[0]))
                    raise PeerDisconnectedError(str(err), addr[0], sock) from err
                except ConnectionError as err:
                    raise PeerDisconnectedError(str(err), addr[0], sock) from err
            elif sock.fileno == -1:
                raise PeerDisconnectedError('invalid fileno', addr[0], sock)

    def disconnect_client(self, ip_address):
        for sock, addr in self.clients:
            if addr[0] == ip_address:
                try:
                    self.clients.remove({sock, addr})
                except ValueError:
                    pass

                sock.close()

                logger.debug('Removed %s from server.', addr)
                return

    def close(self):
        self._stop_event.set()

        # close UDP socket
        self._sock.close()

        self.connection_handler.join()
        for client in self.clients:
            client[0].close()


class Connection(threading.Thread):
    """Uses client and server to communicate and provides a nice interface"""
    def __init__(self, port, announce_ip):
        super().__init__(name='Connection')
        self.port = port
        self.announce_ip = announce_ip

        self.peers = {}
        self.local_ip = self._get_local_ip()
        self._server = Server(port)
        self._sock = None
        self._stop_event = threading.Event()
        self._message_handlers = {}
        self._context_lock = threading.Lock()

        # handlers
        self._on_peer_connected = []
        self._on_peer_disconnected = []
        self._on_peer_changed = []

        self._setup()

    def __enter__(self):
        self._context_lock.acquire()
        caller = getframeinfo(stack()[1][0])
        logger.debug(
            'Connection locked by %s:%d', caller.filename, caller.lineno)

    def __exit__(self, *args):
        self._context_lock.release()
        caller = getframeinfo(stack()[1][0])
        logger.debug(
            'Connection unlocked by %s:%d', caller.filename, caller.lineno)

    def _get_local_ip(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.connect((self.announce_ip, self.port))
        local_ip = sock.getsockname()[0]
        sock.close()
        logger.debug('Local IP address: %s', local_ip)

        return local_ip

    def _setup(self):
        """sets up an UDP socket to listen for and send announcements of new
        peers
        """
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self._sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._sock.bind(('', self.port))

    def _add_peer(self, address):
        try:
            new_peer = Client(address[0], self.port, self._on_peer_changed)
        except socket.error as err:
            logger.error('Could not connect to %s: %s', address, err)
            return

        self.peers[address[0]] = new_peer
        self._sock.sendto(ANNOUNCE_MSG, address)
        handlers = threading.Thread(
            target=self._call_peer_connected_handlers,
            args=(new_peer,), name='connected_event_handlers')
        handlers.setDaemon(True)
        handlers.start()

    def _call_peer_connected_handlers(self, new_peer):
        for handler in self._on_peer_connected:
            try:
                handler(new_peer)
            except Exception:
                logger.exception(
                    'An exception occured when calling '
                    'a peer connected handler')

    def _remove_peer(self, peer_addr):
        try:
            client = self.peers.pop(peer_addr)
            client.close()
        except KeyError:
            pass

        self._server.disconnect_client(peer_addr)

        logger.info('Peer has disconnected: %s', peer_addr)
        logger.debug('Peers still connected: %s', ', '.join(self.peers.keys()))

        for handler in self._on_peer_disconnected:
            try:
                handler(peer_addr)
            except Exception:
                logger.exception(
                    'An exception occured when calling '
                    'a peer disconnected handler')

    def _announce(self):
        """Announce this peer by broadcasting a UDP packet over the local
        network
        """
        try:
            self._sock.sendto(ANNOUNCE_MSG, (self.announce_ip, self.port))
        except (socket.error, OSError, ConnectionError) as err:
            logger.error('Unable to send announcement: %s', err)

    def _handle_announcements(self):
        message = None

        if can_read(self._sock, 0):
            message, address = self._sock.recvfrom((len(ANNOUNCE_MSG)))

        if message == ANNOUNCE_MSG:
            if address[0] not in self.peers and address[0] != self.local_ip:
                logger.info('New peer %s', address[0])
                self._add_peer(address)
            elif address[0] != self.local_ip:
                logger.debug(
                    'New announcement from connected peer %s', address[0])
                self.peers[address[0]].last_announcement = time.time()

    def _handle_messages(self):
        try:
            for client_sock, message in self._server.recv_messages():
                logger.debug('Received from {}: {}'.format(
                    message.addr, message))
                response = self._handle_message(message)
                logger.debug('Sending to {}: {}'.format(
                    message.addr, response))
                response.write(client_sock)
        except PeerDisconnectedError as err:
            self._remove_peer(err.peer_addr)

    def _handle_message(self, message):
        response = None

        if message.mtype in self._message_handlers:
            handler = self._message_handlers[message.mtype]
            return handler(message)

        logger.error(
            'Could not find handler for message "{}"'
            .format(message.mtype))
        return protocol.Message(
            protocol.MSG_ERROR, protocol.CONT_STR,
            protocol.ERR_UNKNOWN_MESSAGE)

    def send_message(self, peer_addr, message, timeout=None):
        """Send message to a peer

        :param peer_addr: IP address of peer as string
        :param message: message object to send
        :param timeout: timout after which to give up
        :returns: response sent by peer
        """
        try:
            peer = self.peers[peer_addr]
        except KeyError:
            raise UnknownPeerError

        logger.debug('Sending to %s: %s', peer_addr, message)

        retry = reversed(range(5))  # TODO: make this unnecessary
        response = None

        while not response and next(retry):
            try:
                peer.send(message)
                response = peer.get_response(timeout=timeout)
            except (ConnectionResetError, socket.error, struct.error) as err:
                self._remove_peer(peer_addr)
                raise PeerDisconnectedError(err)  # TODO: transport error?

        if not response:
            raise Error('No response')

        logger.debug('Received from %s: %s', response.addr, response)
        return response

    def broadcast_message(self, message, timeout=None):
        """Send message to all connected peers"""
        responses = []
        for peer_addr in self.peers.copy():
            try:
                responses.append(
                    self.send_message(
                        peer_addr, message,
                        timeout=timeout))
            except Error as err:
                logger.error('error from %s: %s', peer_addr, err)

        return responses

    def register_handler(self, message, handler):
        """Set new handler for a message defined by a protocol"""
        self._message_handlers[message] = handler

    def add_on_peer_connected(self, handler):
        """Add a handler to execute when a peer has connected"""
        self._on_peer_connected.append(handler)

    def add_on_peer_disconnected(self, handler):
        """Add a handler to execute when a peer has disconnected"""
        self._on_peer_disconnected.append(handler)

    def add_on_peer_changed(self, handler):
        self._on_peer_changed.append(handler)

    def run(self):
        last_announcement_sent = 0

        while not self._stop_event.is_set():
            self._handle_announcements()
            self._handle_messages()

            if time.time() - last_announcement_sent > 10:
                last_announcement_sent = time.time()
                self._announce()

                for peer in list(self.peers.values()):
                    if time.time() - peer.last_announcement > 30:
                        logger.debug(
                            'Announcement from %s overdue by 30 seconds, '
                            'disconnecting peer (%d > 30 (%d))', peer.ip_address, time.time() - peer.last_announcement, peer.last_announcement)
                        self._remove_peer(peer.ip_address)

            time.sleep(0.01)

    def stop(self):
        self._stop_event.set()
        logger.debug('Stopping connection.')

        for peer in self.peers.values():
            peer.close()

        self._server.close()
        self._sock.close()
