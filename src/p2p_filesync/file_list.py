"""Makes accessing file information thread-safe"""
import hashlib
import logging
import os
import sqlite3
from functools import partial
from inspect import getframeinfo, stack
from pathlib import Path, PurePath
from threading import Lock

logger = logging.getLogger(__name__)

CHUNK_SIZE = 1024 * 1024 * 4

CREATE_TABLE_QUERY = (
    'CREATE TABLE IF NOT EXISTS file ('
    'path TEXT PRIMARY KEY,'
    'mtime REAL NOT NULL,'
    'updated_mtime REAL NOT NULL,'
    'file_hash TEXT,'
    'last_hash_time REAL NOT NULL)')


def atomic(func):
    """Makes a method atomic. Calling class needs to have a _lock member"""
    def _decorator(instance, *args, **kwargs):
        lock = vars(instance).get('_lock', None)

        if not lock:
            raise AttributeError('{} has no _lock member'.format(instance))

        with lock:
            return func(instance, *args, **kwargs)
    return _decorator


class DatabaseError(Exception):
    """Something went wrong while interacting with the sqlite database"""

class SqliteWrapper:
    def __init__(self, path):
        self._connection = sqlite3.connect(path, check_same_thread=False)
        self._connection.row_factory = sqlite3.Row
        self._connection.isolation_level = None
        self._lock = Lock()

    def execute(self, *args, **kwargs):
        result = None

        with self._lock, self._connection as cursor:
            try:
                cursor.execute('begin')
                result = cursor.execute(*args, **kwargs)
                cursor.execute('commit')
            except Exception:
                cursor.execute('rollback')
                raise

        return result


class File:
    """Model of a file"""
    def __init__(
            self, connection, path, root, mtime=0,
            updated_mtime=0, file_hash=None, last_hash_time=0):
        self.connection = connection
        self.path = Path(path).as_posix()
        self.root = root

        self.abs_path = Path(self.root) / self.path
        self.mtime = mtime
        # _updated_mtime is used to determine if an update was done by the
        # application itself or not
        self._updated_mtime = updated_mtime
        self._hash = file_hash
        self._last_hash_time = last_hash_time
        self._lock = Lock()

    def __enter__(self):
        self._lock.acquire()

    def __exit__(self, *_):
        self._lock.release()
        try:
            self._updated_mtime = self.get_mtime()
        except FileNotFoundError:
            pass

        self.update()

    @classmethod
    def list(cls, connection, sync_dir):
        for file_data in connection.execute('SELECT * FROM file'):
            kw_args = dict(file_data)
            path = kw_args.pop('path')
            yield File(connection, path, sync_dir, **kw_args)

    @classmethod
    def create(cls, connection, rel_path, sync_dir):
        new_file = cls(connection, rel_path, sync_dir)

        try:
            connection.execute(
                'INSERT INTO file VALUES ('
                '"{path}","{mtime}","{_updated_mtime}",'
                '"{_hash}","{_last_hash_time}")'.format(**vars(new_file)))
        except sqlite3.IntegrityError as err:
            raise FileExistsError(rel_path) from err

        return new_file

    @classmethod
    def get(cls, connection, rel_path, sync_dir):
        raise NotImplementedError

    def update(self):
        query = (
            'UPDATE file SET '
            'path="{path}",mtime={mtime},'
            'updated_mtime={_updated_mtime},'
            'file_hash="{_hash}",last_hash_time={_last_hash_time} '
            'WHERE path="{path}"'.format(**vars(self)))
        # logger.debug(query)
        self.connection.execute(query)

    def updated_by_synchronizer(self):
        """Returns true is the mtime was last changed
        because of this program
        """
        return self._updated_mtime == self.get_mtime()

    def open(self, mode='rb'):
        """Returns an open file object of this file.

        :param mode='rb': file mode
        """
        return self.abs_path.open(mode)

    def remove(self):
        """Removes a file from the system and the database"""
        self.connection.execute(
            'DELETE FROM file WHERE path="{}"'.format(self.path))
        try:
            self.abs_path.unlink()
        except FileNotFoundError:
            # logger.debug('FIle has already been removed %s', self.path)
            pass

    def set_mtime(self, mtime):
        """Set the file modified time"""
        os.utime(str(self.abs_path), (mtime, mtime))
        self.mtime = mtime
        self._updated_mtime = mtime
        self.update()

    def get_mtime(self):
        """Get the file modified time

        :returns: file mtime in unix time
        """
        mtime = self.stat().st_mtime
        if self.mtime != mtime:
            self.mtime = mtime
            self.update()
        return self.mtime

    def get_size(self):
        return self.stat().st_size

    def stat(self):
        return self.abs_path.stat()

    def move(self, dest):
        new_path = Path(self.root) / dest

        if not new_path.parent.exists():
            new_path.parent.mkdir(parents=True, exist_ok=True)

        try:
            self.abs_path.rename(new_path)
        except FileNotFoundError:
            pass

        self.abs_path = new_path
        self.path = Path(dest).as_posix()
        self.update()

    @property
    def hash(self):
        """Get the checksum hash for this file

        :returns: hash as string
        """
        if (not self._hash or
                int(self._last_hash_time) != int(self.get_mtime())):
            logger.debug(
                'File mtime changed. Updating checksum hash for %s',
                self.path)
            file_hash = hashlib.sha256()

            with self.open() as reader:
                for chunk in iter(partial(reader.read, CHUNK_SIZE), b''):
                    file_hash.update(chunk)

            self._hash = file_hash.hexdigest()
            self._last_hash_time = self.get_mtime()
            self.update()

        return self._hash

    def __repr__(self):
        var_gen = ('{}={}'.format(*i) for i in vars(self).items())
        return '<File: {}>'.format(', '.join(var_gen))


class FileList:
    def __init__(self, sync_dir, db_path):
        self.sync_dir = sync_dir
        self.db_path = db_path

        # the context lock is used when there will be multiple operations
        # performed on this instance
        self._context_lock = Lock()
        self._lock = Lock()
        self._files = {}

        logger.info('Loading file list.')
        try:
            self._connection = SqliteWrapper(db_path)
        except sqlite3.OperationalError as err:
            raise DatabaseError(str(err))
        # create schema if not exists
        self._connection.execute(CREATE_TABLE_QUERY)

        for file in File.list(self._connection, self.sync_dir):
            self[file.path] = file

        self.update()

    def __enter__(self):
        self._context_lock.acquire()
        caller = getframeinfo(stack()[1][0])
        logger.debug(
            'File list locked by %s:%d', caller.filename, caller.lineno)

    def __exit__(self, *args):
        self._context_lock.release()
        caller = getframeinfo(stack()[1][0])
        logger.debug(
            'File list unlocked by %s:%d', caller.filename, caller.lineno)

    @atomic
    def __getitem__(self, rel_path):
        if rel_path not in self._files:
            raise FileNotFoundError(rel_path)

        return self._files[rel_path]

    @atomic
    def __setitem__(self, rel_path, file):
        if rel_path in self._files:
            raise FileExistsError(rel_path)

        self._files[rel_path] = file

    @atomic
    def __delitem__(self, rel_path):
        if rel_path not in self._files:
            raise FileNotFoundError(rel_path)

        del self._files[rel_path]

    def __contains__(self, key):
        return key in self._files

    def __iter__(self):
        yield from self._files

    def __len__(self):
        return len(self._files)

    def __sub__(self, other):
        return set(self._files) - set(other)

    def __rsub__(self, other):
        return set(other) - set(self._files)

    def __str__(self):
        return '<FileList: {}>'.format(
            ', '.join(str(f) for f in self.files()))

    def pop(self, key):
        try:
            value = self._files[key]
        except KeyError:
            raise FileNotFoundError

        del self[key]
        return value

    def update(self):
        """Add any unknown files, forget ones that were removed."""
        found_files = []
        logger.info('Updating file list.')

        for root, _, files in os.walk(self.sync_dir, topdown=False):
            rel_root = Path(root).relative_to(self.sync_dir)
            for name in files:
                rel_path = (rel_root / name).as_posix()
                if rel_path not in self._files:
                    self.add(rel_path)

                found_files.append(rel_path)

        for file_path in self - found_files:
            self.remove(file_path)

    def add(self, rel_path):
        """Create a new File object using the relative path

        :param rel_path: relative path to file
        :returns: File object created for rel_path
        """
        new_file = File.create(self._connection, rel_path, self.sync_dir)
        self[rel_path] = new_file
        return new_file

    def files(self):
        """Yields all File objects"""
        yield from self._files.values()

    def items(self):
        yield from self._files.items()

    def move(self, src, dest):
        """Move a file

        :param src: relative source path
        :param dest: relative destination path
        """
        file = self.pop(src)
        file.move(dest)

        try:
            self[dest] = file
        except FileExistsError:
            # file is overwritten
            pass

    def remove(self, rel_path):
        file = self[rel_path]
        file.remove()
        del self[rel_path]

    def to_rel_path(self, path):
        """Makes a relative path from an absolute path

        :returns: relative path
        """
        return PurePath(path).relative_to(self.sync_dir).as_posix()
