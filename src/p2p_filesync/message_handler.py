import json
import logging
import os
import shutil
from pathlib import Path

from . import protocol

logger = logging.getLogger(__name__)


class MessageHandler:
    """Handles messages received by the connection"""
    def __init__(self, sync_dir, connection, file_list):
        self.sync_dir = sync_dir
        self.connection = connection
        self.file_list = file_list

        self._register_handlers()

    def _register_handlers(self):
        self.connection.register_handler(
            protocol.MSG_GET_FILELIST, self.get_file_list)
        self.connection.register_handler(
            protocol.MSG_GET_CHECKSUM, self.get_file_checksum)
        self.connection.register_handler(
            protocol.MSG_GET_TIME, self.get_file_mtime)
        self.connection.register_handler(
            protocol.MSG_SET_TIME, self.set_file_mtime)
        self.connection.register_handler(
            protocol.MSG_GET_FILE, self.get_file)
        self.connection.register_handler(
            protocol.MSG_SEND_FILE, self.push_file)
        self.connection.register_handler(
            protocol.MSG_DELETE_FILE, self.delete_file)
        self.connection.register_handler(
            protocol.MSG_MOVE_FILE, self.move_file)
        self.connection.register_handler(
            protocol.MSG_DELETE_DIR, self.delete_directory)
        self.connection.register_handler(
            protocol.MSG_MOVE_DIR, self.move_directory)

    def get_file_list(self, _):
        """Returns a local file list as requested by another peer.

        The file list returned has de relative path as key and the mtime as
        value.
        """
        file_list = {}

        with self.file_list:
            for file in self.file_list.files():
                file_list[file.path] = file.get_mtime()

        return protocol.Message(
            protocol.MSG_RESPONSE,
            protocol.CONT_JSON,
            json.dumps(file_list))

    def get_file_checksum(self, message):
        """Returns the file checksum as requested by antoher peer"""
        path = message.data

        try:
            file = self.file_list[path]
        except FileNotFoundError:
            logger.error(
                'Can not calculate checksum: File "{}" does not exist.'
                .format(path))
            return protocol.MessageError(data='err_file_not_found')

        with file:
            checksum = file.hash

        return protocol.Message(
            protocol.MSG_RESPONSE, protocol.CONT_STR, data=checksum)

    def get_file_mtime(self, message):
        """Returns the file modified time"""
        path = message.data

        try:
            file = self.file_list[path]
        except FileNotFoundError:
            logger.error(
                'Can not get mtime: File "{}" does not exist'.format([path]))
            return protocol.MessageError(data='err_file_not_found')

        mtime = file.get_mtime()
        return protocol.Message(
            protocol.MSG_RESPONSE, protocol.CONT_STR, str(mtime))

    def get_file(self, message):
        """Upload a file to the requesting peer"""
        path = message.data

        logger.info('Uploading {} to {}'.format(path, message.addr))

        try:
            file = self.file_list[path]
        except FileNotFoundError as err:
            logger.error('file not found: {}'.format(err))
            return protocol.MessageError(data='err_file_not_found')

        return protocol.FileMessage(
            protocol.MSG_RESPONSE, data=file,
            path=file.path, mtime=file.get_mtime(), size=file.get_size())

    def push_file(self, file_msg):
        """Download a file from the requesting peer"""
        path = file_msg.path

        logger.info(
            'Downloading %s of size %s bytes from %s',
            path, file_msg.size, file_msg.addr)

        with self.file_list:
            try:
                file = self.file_list.add(path)
            except FileExistsError:
                logger.debug(
                    'Overwriting %s with version from %s', path, file_msg.peer)
                file = self.file_list[path]

            file.abs_path.parent.mkdir(parents=True, exist_ok=True)
            with file, file.open('wb') as f_obj:
                bytes_recvd = 0

                while bytes_recvd < file_msg.size:
                    chunk_size = min(protocol.BUFF_SIZE,
                                     file_msg.size - bytes_recvd)
                    chunk = file_msg.read(chunk_size)
                    f_obj.write(chunk)
                    bytes_recvd += len(chunk)

                os.fsync(f_obj.fileno())

                file.set_mtime(file_msg.mtime)

        return protocol.MessageOK()

    def delete_file(self, message):
        """Delete a file locally as requested by another peer"""
        path = message.data

        logger.info('Deleting {}'.format(path))

        try:
            with self.file_list:
                self.file_list.remove(path)
        except FileNotFoundError:
            logger.error(
                'Could not remove file "{}": file does not exist.'
                .format(path))
            return protocol.MessageError(data='err_file_not_found')

        return protocol.MessageOK()

    def move_file(self, message):
        """Moves a  file locally"""
        move_info = message.data
        logger.info('Moving %s to %s.', move_info['src_path'], move_info['dest_path'])

        with self.file_list:
            try:
                self.file_list.move(
                    move_info['src_path'], move_info['dest_path'])
            except FileNotFoundError:
                # the file has already been moved
                return protocol.MessageError(data='err_file_not_found')

        return protocol.MessageOK()

    def delete_directory(self, message):
        path = Path(self.sync_dir) / Path(message.data)
        shutil.rmtree(str(path), ignore_errors=True)

        with self.file_list:
            self.file_list.update()

        return protocol.MessageOK()

    def move_directory(self, message):
        move_info = message.data
        os.rename(move_info['src_path'], move_info['dst_path'])

        with self.file_list:
            self.file_list.update()

        return protocol.MessageOK()

    def set_file_mtime(self, message):
        """Set ther modified time for a local file"""
        file_info = message.data
        new_mtime = float(file_info['mtime'])
        logger.info(
            'Updating mtime of {} to {}'
            .format(file_info['path'], new_mtime))

        with self.file_list:
            file = self.file_list[file_info['path']]
            file.set_mtime(new_mtime)

        return protocol.MessageOK()
