"""Graphical interface for this program"""
import faulthandler  # TODO: debug
import ipaddress
import logging
import os
import sys

from PySide2.QtCore import Signal, Slot
from PySide2.QtGui import QIcon
from PySide2.QtWidgets import (
    QAction, QApplication, QFileDialog, QFormLayout, QHBoxLayout, QInputDialog,
    QLabel, QLineEdit, QListWidget, QMainWindow, QMenu, QMessageBox,
    QPushButton, QSystemTrayIcon, QVBoxLayout, QWidget, qApp)

from .connection import Connection
from .file_list import DatabaseError, FileList
from .message_handler import MessageHandler
from .synchronizer import Synchronizer
from .utils import (EXIT_DATABASE_ERROR, EXIT_MISSING_SETTING, EXIT_NOTRAY,
                    EXIT_QTERROR, resource_path, save_config, setup)


logger = logging.getLogger(__name__)


class MainWindow(QMainWindow):
    """Configuration pane"""
    _peer_event_sig = Signal(str, object)

    def __init__(self, config, config_path):
        super().__init__()
        self.config = config
        self.config_path = config_path
        self.file_list = FileList(
            self.config['directory'], self.config['database'])

        self._create_tray_icon()
        self._create_controls()

        self._peer_event_sig.connect(self._handle_peer_event)

        self._start()

        self.setFixedSize(500, 400)
        self.setWindowTitle('P2P file synchronizer')
        self.show()

    def _start(self):
        try:
            self.connection = Connection(
                int(self.config['port']), self.config['announce_address'])
            self.synchronizer = Synchronizer(
                self.config['directory'], self.connection, self.file_list)
            self.message_handler = MessageHandler(
                self.config['directory'], self.connection, self.file_list)
        except KeyError as err:
            logger.error('Missing setting: {}'.format(err))
            sys.exit(EXIT_MISSING_SETTING)

        self.connection.add_on_peer_connected(self.new_peer)
        self.connection.add_on_peer_disconnected(self.peer_disconnected)
        self.connection.add_on_peer_changed(self.peer_changed)
        self.connection.start()

        self.synchronizer.start()

    def _create_tray_icon(self):
        icon_path = resource_path('data/icon.png')

        if not os.path.exists(icon_path):
            logger.error('Could not load system tray icon from %s', icon_path)

        icon = QIcon(icon_path)

        self.tray_icon = QSystemTrayIcon(icon, self)

        self.menu = QMenu(self)

        action_show = QAction('Configure', self.menu)
        action_quit = QAction('Quit', self.menu)

        action_show.triggered.connect(self.show)
        action_quit.triggered.connect(self.exit)

        self.menu.addAction(action_show)
        self.menu.addAction(action_quit)

        self.tray_icon.setContextMenu(self.menu)
        self.tray_icon.show()

    def _create_controls(self):
        w = QWidget(self)
        top_layout = QVBoxLayout(w)
        form_layout = QFormLayout()
        btn_box = QHBoxLayout()
        dir_box = QHBoxLayout()

        self.port_input = QLineEdit(self.config['port'], self)
        self.dir_input = QLineEdit(self.config['directory'], self)
        self.addr_input = QLineEdit(self.config['announce_address'], self)
        self.peer_list = QListWidget(self)
        self.close_btn = QPushButton('Close', self)
        self.apply_btn = QPushButton('Apply', self)

        browse_btn = QPushButton('Browse')
        peer_list_lbl = QLabel('Peers connected:', self)

        self.apply_btn.clicked.connect(self.apply_settings)
        self.close_btn.clicked.connect(self.hide)
        browse_btn.clicked.connect(self.pick_directory)

        dir_box.addWidget(self.dir_input)
        dir_box.addWidget(browse_btn)

        form_layout.addRow('Port', self.port_input)
        form_layout.addRow('Watch directory', dir_box)
        form_layout.addRow('Announce address', self.addr_input)
        top_layout.addLayout(form_layout)
        top_layout.addStretch(1)

        # self.peer_list.setEnabled(False)
        top_layout.addWidget(peer_list_lbl)
        top_layout.addWidget(self.peer_list)

        self.apply_btn.setDefault(True)

        btn_box.addWidget(self.close_btn)
        btn_box.addWidget(self.apply_btn)
        btn_box.addStretch(1)
        top_layout.addLayout(btn_box)

        self.setCentralWidget(w)
        self.setLayout(top_layout)

    def _update_peer_list(self):
        self.peer_list.clear()
        for peer in self.connection.peers.values():
            item_text = '{} ({})'.format(
                peer.ip_address,
                'synced' if peer.synced else 'not synced')
            self.peer_list.addItem(item_text)

    def pick_directory(self):
        """Opens a file dialog enabling the user to pick a directory"""
        new_dir = QFileDialog.getExistingDirectory(
            self, "Open Directory",
            options=QFileDialog.ShowDirsOnly
            | QFileDialog.DontResolveSymlinks)
        self.dir_input.setText(new_dir)  # TODO: reset text to old config value when user presses cancel

    def apply_settings(self):
        try:
            new_port = int(self.port_input.text())
        except ValueError:
            QMessageBox.critical(
                self, 'Port value', 'Port needs to be numeric!')
            return

        if not 0 <= new_port <= 65535:
            QMessageBox.critical(
                self, 'Port value', 'Port needs to be in range 0-65535!')
            return

        self.config['port'] = str(new_port)

        new_dir = self.dir_input.text()
        if not os.path.exists(new_dir):
            QMessageBox.critical(
                self, 'Directory value', 'Directory does not exist!')
            return

        if self.config['directory'] != new_dir:
            self.file_list.sync_dir = new_dir
            with self.file_list:
                self.file_list.update()

        self.config['directory'] = new_dir

        new_ip = self.addr_input.text()

        try:
            self.config['announce_address'] = str(ipaddress.ip_address(new_ip))
        except ValueError:
            QMessageBox.critical(
                self,
                'IP address value',
                '{} is not a valid IP address!'.format(new_ip))
            return

        self.stop()
        self.peer_list.clear()

        save_config(self.config_path, self.config)
        self._start()

    def new_peer(self, client):
        self._peer_event_sig.emit('connect', client)

    def peer_disconnected(self, peer_addr):
        self._peer_event_sig.emit('disconnect', peer_addr)

    def peer_changed(self, client):
        self._peer_event_sig.emit('changed', client)

    @Slot(str, object)
    def _handle_peer_event(self, event, arg):
        logger.debug('Connection event "%s", arg: %s', event, arg)
        if event == 'connect':
            client = arg
            self.tray_icon.showMessage(
                'Peer connected', client.ip_address)
        elif event == 'disconnect':
            peer_addr = arg
            self.tray_icon.showMessage('Peer disconnected', peer_addr)

        self._update_peer_list()

    def stop(self):
        self.synchronizer.stop()
        self.synchronizer.join()
        self.connection.stop()
        self.connection.join()

    def exit(self):
        self.stop()
        qApp.exit()


def main():
    config, path = setup()

    app = QApplication(sys.argv)
    app.setQuitOnLastWindowClosed(False)

    sys._excepthook = sys.excepthook

    def exception_hook(exctype, value, traceback):
        print(exctype, value, traceback)
        sys._excepthook(exctype, value, traceback)
        sys.exit(EXIT_QTERROR)
    sys.excepthook = exception_hook

    if not QSystemTrayIcon.isSystemTrayAvailable():
        QMessageBox.critical(None, 'Systray',
                             'No system tray available.')
        sys.exit(EXIT_NOTRAY)

    if not config['directory']:
        config['directory'], result = QInputDialog.getText(
            None, 'No watch directory',
            'Enter the directory to watch:')
        if result:
            config = save_config(path, config)['settings']
        else:
            sys.exit(EXIT_MISSING_SETTING)

    try:
        MainWindow(config, path)
    except DatabaseError:
        QMessageBox.critical(
            None, 'Database Error',
            'Unable to open database {}'.format(config['database']))
        sys.exit(EXIT_DATABASE_ERROR)

    sys.exit(app.exec_())


if __name__ == '__main__':
    faulthandler.enable(file=sys.stdout)
    main()
