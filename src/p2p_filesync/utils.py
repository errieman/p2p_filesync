import argparse
import configparser
import logging
import os
import select
import sys
import time
from pathlib import Path

# exit error codes
EXIT_MISSING_SECTION = 'Missing section in config'
EXIT_MISSING_SETTING = 'Missing setting in config'
EXIT_NOTRAY = 'No system tray available'
EXIT_QTERROR = 'QT threw an error'
EXIT_DATABASE_ERROR = 'Database could not be opened'


def save_config(path, config=None):
    """Updates config with provided dictionary.

    If config is omitted a default config is saved.

    :param path: config file path
    :param config=None: dictionary containing new config
    :returns: updated ConfigParser instance
    """
    base_dir = os.path.dirname(path)
    if not os.path.exists(base_dir):
        os.makedirs(base_dir)

    parser = configparser.ConfigParser()
    parser['settings'] = config or {
        'port': 1337,
        'announce_address': '0.0.0.0',
        'directory': '',
        'file_list_path': ''
    }

    with open(path, 'w') as config_file:
        parser.write(config_file)

    return parser


def load_config(paths):
    config = configparser.ConfigParser()
    used_config = config.read(paths)

    if not used_config:
        print(
            'No readable configuration file found in {}'
            .format(', '.join(paths)))
        config = save_config(paths[0])
        used_config = paths

    print('Using config "{}"'.format(used_config))

    if 'settings' not in config.sections():
        print('Missing section "settings" in config file.')
        sys.exit(EXIT_MISSING_SECTION)

    return (config['settings'], used_config[0])


def parse_args():
    parser = argparse.ArgumentParser(
        description='Peer to peer file synchronization')
    parser.add_argument(
        '--config', help='Location of the configuration file',
        action='store', dest='config')
    parser.add_argument(
        '--debug', help='Enable remote ptvsd debugging server',
        action='store_true', default=False)

    return parser.parse_args()


def setup():
    # disable debug messages from watchdog
    watchdog_logger = logging.getLogger('watchdog')
    watchdog_logger.setLevel(logging.ERROR)
    args = parse_args()
    user_home = Path.home()
    config_root = None
    config_paths = []

    if args.debug:
        import ptvsd
        ptvsd.enable_attach()  # default port: 5678
        input('Press enter to continue.')

    if args.config:
        config_paths = [args.config]
    elif sys.platform == 'linux':
        config_root = user_home / '.config/p2p_filesync/'
        config_paths.append(str(config_root / 'config.ini'))
    elif sys.platform == 'win32':
        config_root = user_home / 'AppData/Local/p2p_filesync'
        config_paths.append(str(config_root / 'config.ini'))

    config, path = load_config(config_paths)

    if 'log_file' in config:
        logging.basicConfig(
            filename=config['log_file'],
            level=logging.DEBUG,
            format='%(asctime)s -  %(levelname)s - %(name)s - %(message)s')
    else:
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s -  %(levelname)s - %(name)s - %(message)s')

    # default to config directory as database directory
    if 'database' not in config:
        if config_root:
            config['database'] = str(config_root / 'file_list.db')
        else:
            print('Missing database setting.')
            sys.exit(1)

    return config, path


def can_read(sock, timeout):
    """Check if socket is readable

    :param sock: socket to test
    :param timeout: time after which to give up
    :returns: whether the socket is readable
    """
    return select.select([sock], [], [], timeout)[0]


def wait_for_write(file):
    previous_size = -1

    while previous_size != file.get_size():
        previous_size = file.get_size()
        time.sleep(1)


def resource_path(path):
    path = str(Path(path))
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, path)
    return path
